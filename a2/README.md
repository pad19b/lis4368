> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Patrick Duke

### Assignment A2 Requirements:

*Two-Parts:*

1. Write and run a Java Servlet
2. Write and run a Database Servlet

#### README.md file should include the following items:

* Hello Directory
* Hello index.html
* Servlet Compilation and Usage
* Database Connectivity Using Servlet

#### Assignment Screenshots:

*Screenshot of Hello Directory*:

![Hello Directory Screenshot](img/SS1.png)

*Screenshot of Hello index.html*:

![Hello index.html Screenshot](img/SS2.png)

*Screenshot of Servlet Compilation and Usage*:

![Servlet Compilation and Usage Screenshot](img/SS3.png)

*Database Connectivity Using Servlets*:

| querybook.html                                                 | query result                                                   |  
| -------------------------------------------------------------- | -------------------------------------------------------------- |
| ![Database Connectivity Using Servlet Screenshot](img/SS4.png) | ![Database Connectivity Using Servlet Screenshot](img/SS5.png) |

*Screenshot of LIS4369 A2 Web App*:

![LIS4369 A2 Web App Screenshot](img/index.png)
