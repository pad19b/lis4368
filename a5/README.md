> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Patrick Duke

### Assignment A5 Requirements:

*Four Parts:*

1. Continue MVC setup from A4
2. Create data handling servlets
3. Skillsets
4. Chapter Questions

#### README.md file should include the following items:

* Screenshot valid user entry
* Screenshot of passed validation (thanks.jsp)
* Screenshot of Database Entry
* Screenshot of Skillsets

#### Assignment Screenshots:

*Screenshot of valid user entry*:

![valid user entry Screenshot](img/ValidEntry.png)

*Screenshot of passed validation*:

![passed validation Screenshot](img/Passed.png)

*Screenshot of Database Entry*:

![Database Entry Screenshot](img/Database.png)

*Screenshot of SS13: Number Swap*:

![SS13: Number Swap Screenshot](img/NumberSwap.png)

*Screenshot of SS14: Largest Numbers*:

![SS14: Largest Numbers Screenshot](img/LargestNumbers.png)

*Screenshots of SS15: Simple Calculator*:

![SS15: Simple Calculato Screenshot](img/SimpleCalculator.png)
