> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Patrick Duke

### Assignment P2 Requirements:

*Three-Parts:*

1. Finish MVC setup from A5
2. Modify data handling servlets to include add, update, and delete funcionality
3. Chapter Questions

#### README.md file should include the following items:

* Screenshot valid user entry
* Screenshot of passed validation (thanks.jsp)
* Screenshot of data display
* Screenshot of modified form
* Screenshot of modified data
* Screenshot of delete warning
* Screenshot of database changes

#### Assignment Screenshots:

*Screenshot of valid user entry*:

![valid user entry Screenshot](img/ValidEntry.png)

*Screenshot of passed validation*:

![passed validation Screenshot](img/Passed.png)

*Screenshot of data display*:

![data display Screenshot](img/DataDisplay.png)

*Screenshot of modified form*:

![modified form Screenshot](img/ModifiedForm.png)

*Screenshot of modified data*:

![modified data Screenshot](img/ModifiedData.png)

*Screenshot of delete warning*:

![delete warning Screenshot](img/DeleteWarning.png)

*Screenshots of database changes*:

![database chanhes Screenshot](img/DatabaseChanges1.png)

![database chanhes Screenshot](img/DatabaseChanges2.png)

![database chanhes Screenshot](img/DatabaseChanges3.png)

![database chanhes Screenshot](img/DatabaseChanges4.png)
