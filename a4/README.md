> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Patrick Duke

### Assignment A4 Requirements:

*Four Parts:*

1. Set up a MVC
2. Add server-side validation
3. Skillsets
4. Chapter Questions

#### README.md file should include the following items:

* Screenshot of failed validation
* Screenshot of passed validation
* Screenshot of Skillsets

#### Assignment Screenshots:

*Screenshot of failed validation*:

![failed validation Screenshot](img/failed.png)

*Screenshot of passed validation*:

![passed validation Screenshot](img/passed.png)

*Screenshot of SS10: Count Characters*:

![SS10: Count Characters Screenshot](img/CountCharacter.png)

*Screenshot of SS11: File Write/Word Count*:

![SS11: File Write/Word Count Screenshot](img/FileWrite.png)

*Screenshots of SS12: ASCII App*:

![SS12: ASCII App Screenshot](img/ASCIIApp1.png)

![SS12: ASCII App Screenshot](img/ASCIIApp2.png)

