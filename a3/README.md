> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Patrick Duke

### Assignment A3 Requirements:

*Three Parts:*

1. Database creation and forward engineer (locally)
2. Database ERD
3. Update a3/index.jsp

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of a3/index.jsp
* Links to:
    * a3.mwb
    * a3.sql

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/ERD.png)

*Screenshot of a3/index.jsp*:

![a3/index.jsp Screenshot](img/index.png)

#### Links:

*Link to a3.mwb*
[a3.mwb File](docs/a3.mwb "A3 ERD in .mwb format")

*Link to a3.sql*
[a3.sql File](docs/a3.sql "A3 SQL Script")
