> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web App Development

## Patrick Duke

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Screenshots of installs
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationslocations)
    - provide git command descriptions 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Write and run a Java Servlet
    - Write and run a Database Servlet
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create database and forward engineer locally
    - Provide screenshot of ERD
    - Provide screenshot of a3/index.jsp
    - Provide .mwb link and .sql link

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Update p1/index.jsp with customer table fields
    - Update p1/index.jsp with data validation
    - Provide Screenshots of main index.jsp and p1/index.jsp 
    - Chapter Questions 

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Set up a MVC
    - Add server-side validation
    - Skillsets
    - Chapter Questions

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Continue MVC setup from A4
    - Create data handling servlets
    - Skillsets
    - Chapter Questions

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Finish MVC setup from A5
    - Modify data handling servlets to include add, update, and delete funcionality
    - Chapter Questions

