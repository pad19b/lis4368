> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Patrick Duke

### Assignment A1 Requirements:

*Three-Parts:*

1. Distrbution Version Control with Git and Bitcucket Setup (Git descriptions and repo links)
2. Java/Server Development Install
3. Chapter Questions

#### README.md file should include the following items:

* Screenshot of running JDK "java Hello"
* Screenshot of running http//localhost9999
* Link to local lis4368 web app
* git commands with short descriptions
* the bitbucket totrial (bitbucketstationlocations)


#### Assignment Screenshots:

*Screenshot of running JDK - "java Hello" Program*:

!["java Hello" Program](img/Helloworld.png)

*Screenshot of running JDK - http//localhost9999*:

![Tomcat Server](img/Tomcat.png)

#### Link to Local LIS4369 Web App

[local lis4368 web app](http://localhost:9999/lis4368/index.jsp "lis4368 web app")


> #### Git commands w/short descriptions:
>
>1. git init - create an empty Git repository or reinitialize an existing one
>2. git status - show the working tree status
>3. git add - add file contents to the index
>4. git commit - record changes to the repository
>5. git push - update remote refs along with associated objects
>6. git pull - fetch from and integrate with another repository or a local branch
>7. git log - show commit logs


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[Bitbucket Station Locations Link](https://bitbucket.org/pad19b/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
