> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Patrick Duke

### Assignment P1 Requirements:

*Four Parts:*

1. Update p1/index.jsp with customer table fields
2. Update p1/index.jsp with data validation
3. Provide Screenshots of main index.jsp and p1/index.jsp 
4. Chapter questions

#### README.md file should include the following items:

* Screenshot of main index.jsp
* Screenshot of p1/index.jsp with data validation failed
* Screenshot of p1/index.jsp with data validation success

#### Assignment Screenshots:

*Screenshot of main index.jsp*:

![AMPPS Installation Screenshot](img/main.png)

*Screenshot of p1/index.jsp with data validation failed*:

![JDK Installation Screenshot](img/failed.png)

*Screenshot of p1/index.jsp with data validation success*:

![Android Studio Installation Screenshot](img/passed.png)
